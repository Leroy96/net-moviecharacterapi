﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieCharacterAPI.Migrations
{
    public partial class SeedingControllers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_MovieCharacters",
                table: "MovieCharacters");

            migrationBuilder.DropIndex(
                name: "IX_MovieCharacters_CharacterId",
                table: "MovieCharacters");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MovieCharacters",
                table: "MovieCharacters",
                columns: new[] { "CharacterId", "MovieId" });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "Alias", "Gender", "Name", "Picture" },
                values: new object[,]
                {
                    { 1, "Iron Man", "Male", "Tony Stark", "https://www.manners.nl/wp-content/uploads/2020/05/Iron-Man-3-Tony-Stark-record.jpg" },
                    { 2, "Black Widow", "Female", "Natasha Romanoff", "https://static.posters.cz/image/1300/posters/black-widow-unfinished-business-i122138.jpg" },
                    { 3, "Spiderman", "Male", "Peter Parker", "https://upload.wikimedia.org/wikipedia/en/0/0f/Tom_Holland_as_Spider-Man.jpg" },
                    { 4, "The boy who lived", "Male", "Harry Potter", "https://images0.persgroep.net/rcs/etIadxKYKlQFg8IRSzdu6-yxlA8/diocontent/207989891/_fitwidth/694/?appId=21791a8992982cd8da851550a453bd7f&quality=0.8" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "The Marvel Cinematic Universe (MCU) is an American media franchise and shared universe centered on a series of superhero films produced by Marvel Studios. The films are based on characters that appear in American comic books published by Marvel Comics. The franchise also includes television series, short films, digital series, and literature. The shared universe, much like the original Marvel Universe in comic books, was established by crossing over common plot elements, settings, cast, and characters.", "Marvel Cinematic Universe" },
                    { 2, "Harry Potter is a film series based on the eponymous novels by J. K. Rowling. The series is distributed by Warner Bros. and consists of eight fantasy films, beginning with Harry Potter and the Philosopher's Stone (2001) and culminating with Harry Potter and the Deathly Hallows – Part 2 (2011). A spin-off prequel series that is planned to consist of five films started with Fantastic Beasts and Where to Find Them (2016), marking the beginning of the Wizarding World shared media franchise.", "Harry Potter" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "Picture", "ReleaseYear", "Title", "Trailer" },
                values: new object[] { 1, "Jon Watts", 1, "Sci-Fi, Action", "https://images.pathe-thuis.nl/12025_450x640.jpg", 2017, "Spider-Man: Homecoming", "https://www.youtube.com/watch?v=n9DwoQ7HWvI&ab_channel=MovieclipsTrailers" });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "Picture", "ReleaseYear", "Title", "Trailer" },
                values: new object[] { 2, "Joss Whedon", 1, "Sci-Fi, Action, Adventure", "https://media.s-bol.com/m7krYyxWjlz9/3lNORVA/550x781.jpg", 2012, "The Avengers", "https://www.youtube.com/watch?v=eOrNdBpGMv8&ab_channel=MarvelEntertainment" });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "Picture", "ReleaseYear", "Title", "Trailer" },
                values: new object[] { 3, "Mike Newell", 2, "Fantasy, Adventure", "https://images.pathe-thuis.nl/15295_450x640.jpg", 2005, "Harry Potter and the Goblet of Fire", "https://www.youtube.com/watch?v=3EGojp4Hh6I&ab_channel=MovieclipsClassicTrailers" });

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacters_MovieId",
                table: "MovieCharacters",
                column: "MovieId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_MovieCharacters",
                table: "MovieCharacters");

            migrationBuilder.DropIndex(
                name: "IX_MovieCharacters_MovieId",
                table: "MovieCharacters");

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.AddPrimaryKey(
                name: "PK_MovieCharacters",
                table: "MovieCharacters",
                columns: new[] { "MovieId", "CharacterId" });

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacters_CharacterId",
                table: "MovieCharacters",
                column: "CharacterId");
        }
    }
}
