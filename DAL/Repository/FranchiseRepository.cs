﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.DTOs.FranchiseDTOs;
using MovieCharacterAPI.Interfaces;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Dal.Repository
{
    public class FranchiseRepository : IFranchiseRepository
    {
        // Variable for the DbContext.
        private readonly MovieCharacterDbContext _context;
        // Variable mapper for mapping the DTOs.
        private readonly IMapper _mapper;
        /// <summary>
        /// Constructor for the FranchiseRepository.
        /// </summary>
        /// <param name="context">Db context</param>
        /// <param name="mapper">Imapper</param>
        public FranchiseRepository(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        /// <summary>
        /// Get all Franchises.
        /// </summary>
        /// <returns>A list of FranchiseDto objects.</returns>
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchisesAsync()
        {
            // Assign it to the domain object
            var domainFranchises = await _context.Franchises.ToListAsync();
            // Map domainFranchise with FranchiseReadDTO
            var dtoFranchise = _mapper.Map<List<FranchiseReadDTO>>(domainFranchises);
            return dtoFranchise;
        }
        /// <summary>
        /// Get a Franchise by Id.
        /// </summary>
        /// <param name="id">Id of the FranchiseDto object.</param>
        /// <returns>A FranchiseDto object.</returns>
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchiseAsync(int id)
        {
            var domainFranchise = await _context.Franchises.FindAsync(id);
            // Map domainFranchise with FranchiseReadDTO
            var FranchiseReadDto = _mapper.Map<FranchiseReadDTO>(domainFranchise);
            return FranchiseReadDto;
        }
        /// <summary>
        /// Create a Franchise object.
        /// </summary>
        /// <param name="FranchiseDto"></param>
        /// <returns>The new FranchiseDto object.</returns>
        public async Task<ActionResult<FranchiseReadDTO>> PostFranchiseAsync(FranchiseCreateDTO FranchiseDto)
        {
            // Map domainFranchise with FranchiseCreateDTO
            var domainFranchise = _mapper.Map<Franchise>(FranchiseDto);
            _context.Franchises.Add(domainFranchise);
            await _context.SaveChangesAsync();
            // Map FranchiseReadDTO with Franchise
            FranchiseReadDTO newFranchise = _mapper.Map<FranchiseReadDTO>(domainFranchise);
            return newFranchise;
        }
        public async Task<ActionResult<FranchiseUpdateDTO>> PutFranchiseAsync(int id, FranchiseUpdateDTO FranchiseDto)
        {
            var domainFranchise = _context.Franchises.Find(id);
            domainFranchise.Name = FranchiseDto.Name;
            //// Map domainFranchise with franchiseReadDTO
            var FranchiseUpdateDto = _mapper.Map<FranchiseUpdateDTO>(domainFranchise);
            await _context.SaveChangesAsync();
            return FranchiseUpdateDto;
        }
        /// <summary>
        /// Delete a Franchise by Id.
        /// </summary>
        /// <param name="id">Id of the FranchiseDto object.</param>
        /// <returns></returns>
        public async Task<ActionResult<FranchiseDeleteDTO>> DeleteFranchiseAsync(int id)
        {
            var domainFranchise = await _context.Franchises.FindAsync(id);
            // Map domainFranchise with FranchiseDeleteDTO
            var FranchiseDeleteDto = _mapper.Map<FranchiseDeleteDTO>(domainFranchise);
            _context.Franchises.Remove(domainFranchise);
            await _context.SaveChangesAsync();
            return FranchiseDeleteDto;
        }
    }
}