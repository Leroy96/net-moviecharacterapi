﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.DTOs.MovieDTOs;
using MovieCharacterAPI.Interfaces;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Dal.Repository
{
    public class MovieRepository : IMovieRepository
    {
        // Variable for the DbContext.
        private readonly MovieCharacterDbContext _context;
        // Variable mapper for mapping the DTOs.
        private readonly IMapper _mapper;
        /// <summary>
        /// Constructor for the MovieRepository.
        /// </summary>
        /// <param name="context">Db context</param>
        /// <param name="mapper">Imapper</param>
        public MovieRepository(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        /// <summary>
        /// Get all Movies.
        /// </summary>
        /// <returns>A list of MovieDto objects.</returns>
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMoviesAsync()
        {
            // Assign it to the domain object
            var domainMovies = await _context.Movies.ToListAsync();
            // Map domainMovie with MovieReadDTO
            var dtoMovie = _mapper.Map<List<MovieReadDTO>>(domainMovies);
            return dtoMovie;
        }
        /// <summary>
        /// Get a Movie by Id.
        /// </summary>
        /// <param name="id">Id of the MovieDto object.</param>
        /// <returns>A MovieDto object.</returns>
        public async Task<ActionResult<MovieReadDTO>> GetMovieAsync(int id)
        {
            var domainMovie = await _context.Movies.FindAsync(id);
            // Map domainMovie with MovieReadDTO
            var MovieReadDto = _mapper.Map<MovieReadDTO>(domainMovie);
            return MovieReadDto;
        }
        /// <summary>
        /// Create a Movie object.
        /// </summary>
        /// <param name="MovieDto"></param>
        /// <returns>The new MovieDto object.</returns>
        public async Task<ActionResult<MovieReadDTO>> PostMovieAsync(MovieCreateDTO MovieDto)
        {
            // Map domainMovie with MovieCreateDTO
            var domainMovie = _mapper.Map<Movie>(MovieDto);
            _context.Movies.Add(domainMovie);
            await _context.SaveChangesAsync();
            // Map MovieReadDTO with Movie
            MovieReadDTO newMovie = _mapper.Map<MovieReadDTO>(domainMovie);
            return newMovie;
        }
        public async Task<ActionResult<MovieUpdateDTO>> PutMovieAsync(int id, MovieUpdateDTO MovieDto)
        {
            var domainMovie = _context.Movies.Find(id);
            domainMovie.Title = MovieDto.Title;
            //// Map domainMovie with MovieReadDTO
            var MovieUpdateDto = _mapper.Map<MovieUpdateDTO>(domainMovie);
            await _context.SaveChangesAsync();
            return MovieUpdateDto;
        }
        /// <summary>
        /// Delete a Movie by Id.
        /// </summary>
        /// <param name="id">Id of the MovieDto object.</param>
        /// <returns></returns>
        public async Task<ActionResult<MovieDeleteDTO>> DeleteMovieAsync(int id)
        {
            var domainMovie = await _context.Movies.FindAsync(id);
            // Map domainMovie with MovieDeleteDTO
            var MovieDeleteDto = _mapper.Map<MovieDeleteDTO>(domainMovie);
            _context.Movies.Remove(domainMovie);
            await _context.SaveChangesAsync();
            return MovieDeleteDto;
        }
    }
}