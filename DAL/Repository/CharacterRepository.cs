﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.DTOs.CharacterDTOs;
using MovieCharacterAPI.Interfaces;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Dal.Repository
{
    public class CharacterRepository : ICharacterRepository
    {
        // Variable for the DbContext.
        private readonly MovieCharacterDbContext _context;
        // Variable mapper for mapping the DTOs.
        private readonly IMapper _mapper;
        /// <summary>
        /// Constructor for the CharacterRepository.
        /// </summary>
        /// <param name="context">Db context</param>
        /// <param name="mapper">Imapper</param>
        public CharacterRepository(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        /// <summary>
        /// Get all characters.
        /// </summary>
        /// <returns>A list of characterDto objects.</returns>
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersAsync()
        {
            // Assign it to the domain object
            var domainCharacters = await _context.Characters.ToListAsync();
            // Map domainCharacter with CharacterReadDTO
            var dtoCharacter = _mapper.Map<List<CharacterReadDTO>>(domainCharacters);
            return dtoCharacter;
        }
        /// <summary>
        /// Get a character by Id.
        /// </summary>
        /// <param name="id">Id of the characterDto object.</param>
        /// <returns>A characterDto object.</returns>
        public async Task<ActionResult<CharacterReadDTO>> GetCharacterAsync(int id)
        {
            var domainCharacter = await _context.Characters.FindAsync(id);
            // Map domainCharacter with characterReadDTO
            var characterReadDto = _mapper.Map<CharacterReadDTO>(domainCharacter);
            return characterReadDto;
        }
        /// <summary>
        /// Create a character object.
        /// </summary>
        /// <param name="characterDto"></param>
        /// <returns>The new characterDto object.</returns>
        public async Task<ActionResult<CharacterReadDTO>> PostCharacterAsync(CharacterCreateDTO characterDto)
        {
            // Map domainCharacter with characterCreateDTO
            var domainCharacter = _mapper.Map<Character>(characterDto);
            _context.Characters.Add(domainCharacter);
            await _context.SaveChangesAsync();
            // Map characterReadDTO with character
            CharacterReadDTO newCharacter = _mapper.Map<CharacterReadDTO>(domainCharacter);
            return newCharacter;
        }
        public async Task<ActionResult<CharacterUpdateDTO>> PutCharacterAsync(int id, CharacterUpdateDTO characterDto)
        {
            var domainCharacter = _context.Characters.Find(id);
            domainCharacter.Name = characterDto.Name;
            //// Map domainFranchise with franchiseReadDTO
            var characterUpdateDto = _mapper.Map<CharacterUpdateDTO>(domainCharacter);
            await _context.SaveChangesAsync();
            return characterUpdateDto;
        }
        /// <summary>
        /// Delete a character by Id.
        /// </summary>
        /// <param name="id">Id of the characterDto object.</param>
        /// <returns></returns>
        public async Task<ActionResult<CharacterDeleteDTO>> DeleteCharacterAsync(int id)
        {
            var domainCharacter = await _context.Characters.FindAsync(id);
            // Map domainCharacter with characterDeleteDTO
            var characterDeleteDto = _mapper.Map<CharacterDeleteDTO>(domainCharacter);
            _context.Characters.Remove(domainCharacter);
            await _context.SaveChangesAsync();
            return characterDeleteDto;
        }
    }
}