﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.DTOs.MovieDTOs
{
    public class MovieCreateDTO
    {
        // Fields
        [MaxLength(100)] public string Title { get; set; }
        [MaxLength(50)] public string Genre { get; set; }
        [MaxLength(4)] public int ReleaseYear { get; set; }
        [MaxLength(50)] public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
        // Relationships
        public int FranchiseId { get; set; }
    }
}
