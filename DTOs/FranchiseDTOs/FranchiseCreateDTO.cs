﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.DTOs.FranchiseDTOs
{
    public class FranchiseCreateDTO
    {
        // Fields
        [MaxLength(50)] public string Name { get; set; }
        public string Description { get; set; }
    }
}
