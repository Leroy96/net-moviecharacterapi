﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.DTOs.FranchiseDTOs
{
    public class FranchiseReadDTO
    {
        // Primary key (PK)
        public int Id { get; set; }
        // Fields
        [MaxLength(50)] public string Name { get; set; }
        public string Description { get; set; }
    }
}
