﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.DTOs.CharacterDTOs
{
    public class CharacterCreateDTO
    {
        // Fields
        [MaxLength(50)] public string Name { get; set; }
        [MaxLength(50)] public string Alias { get; set; }
        [MaxLength(20)] public string Gender { get; set; }
        public string Picture { get; set; }
    }
}
