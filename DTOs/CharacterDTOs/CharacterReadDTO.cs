﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MovieCharacterAPI.Models.Domain;

namespace MovieCharacterAPI.DTOs.CharacterDTOs
{
    public class CharacterReadDTO
    {
        // Primary key (PK)
        public int Id { get; set; }
        // Fields
        [MaxLength(50)] public string Name { get; set; }
        [MaxLength(50)] public string Alias { get; set; }
        [MaxLength(20)] public string Gender { get; set; }
        public string Picture { get; set; }
    
    }
}
