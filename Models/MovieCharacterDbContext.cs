﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Models.Domain;

namespace MovieCharacterAPI.Models
{
    public class MovieCharacterDbContext : DbContext
    {

        public MovieCharacterDbContext(DbContextOptions options) : base(options)
        {
        }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<MovieCharacter> MovieCharacters { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Character>().HasData(SeedHelper.GetCharacterSeeds());
            modelBuilder.Entity<Franchise>().HasData(SeedHelper.GetFranchiseSeeds());
            modelBuilder.Entity<Movie>().HasData(SeedHelper.GetMovieSeeds());

            modelBuilder.Entity<MovieCharacter>().HasKey(cm => new { cm.CharacterId, cm.MovieId });
        }
    }
}
