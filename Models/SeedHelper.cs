﻿using System.Collections.Generic;
using MovieCharacterAPI.Models.Domain;

namespace MovieCharacterAPI.Models
{
    public class SeedHelper
    {
        /// <summary>
        /// Seed data for Characters
        /// </summary>
        /// <returns>A list of Characters</returns>
        public static IEnumerable<Character> GetCharacterSeeds()
        {
            var characters = new List<Character>()
            {
               new Character { Id = 1, Name = "Tony Stark", Alias = "Iron Man", Gender = "Male", Picture = "https://www.manners.nl/wp-content/uploads/2020/05/Iron-Man-3-Tony-Stark-record.jpg" },
               new Character { Id = 2, Name = "Natasha Romanoff", Alias = "Black Widow", Gender = "Female", Picture = "https://static.posters.cz/image/1300/posters/black-widow-unfinished-business-i122138.jpg" },
               new Character { Id = 3, Name = "Peter Parker", Alias = "Spiderman", Gender = "Male", Picture = "https://upload.wikimedia.org/wikipedia/en/0/0f/Tom_Holland_as_Spider-Man.jpg" },
               new Character { Id = 4, Name = "Harry Potter", Alias = "The boy who lived", Gender = "Male", Picture = "https://images0.persgroep.net/rcs/etIadxKYKlQFg8IRSzdu6-yxlA8/diocontent/207989891/_fitwidth/694/?appId=21791a8992982cd8da851550a453bd7f&quality=0.8" }
            };
            return characters;
        }

        /// <summary>
        /// Seed data for Franchises
        /// </summary>
        /// <returns>A list of Franchises</returns>
        public static IEnumerable<Franchise> GetFranchiseSeeds()
        {
            var franchises = new List<Franchise>()
            {
                 new Franchise { Id = 1, Name = "Marvel Cinematic Universe", Description = "The Marvel Cinematic Universe (MCU) is an American media franchise and shared universe centered on a series of superhero films produced by Marvel Studios. The films are based on characters that appear in American comic books published by Marvel Comics. The franchise also includes television series, short films, digital series, and literature. The shared universe, much like the original Marvel Universe in comic books, was established by crossing over common plot elements, settings, cast, and characters." },
                 new Franchise { Id = 2, Name = "Harry Potter", Description = "Harry Potter is a film series based on the eponymous novels by J. K. Rowling. The series is distributed by Warner Bros. and consists of eight fantasy films, beginning with Harry Potter and the Philosopher's Stone (2001) and culminating with Harry Potter and the Deathly Hallows – Part 2 (2011). A spin-off prequel series that is planned to consist of five films started with Fantastic Beasts and Where to Find Them (2016), marking the beginning of the Wizarding World shared media franchise." }
            };
            return franchises;
        }

        /// <summary>
        /// Seed data for Movies
        /// </summary>
        /// <returns>A list of Movies</returns>
        public static IEnumerable<Movie> GetMovieSeeds()
        {
            var movies = new List<Movie>()
            {
                new Movie { Id = 1, Title = "Spider-Man: Homecoming", Genre = "Sci-Fi, Action", ReleaseYear = 2017, Director = "Jon Watts", Picture = "https://images.pathe-thuis.nl/12025_450x640.jpg", Trailer = "https://www.youtube.com/watch?v=n9DwoQ7HWvI&ab_channel=MovieclipsTrailers", FranchiseId = 1 },
                new Movie { Id = 2, Title = "The Avengers", Genre = "Sci-Fi, Action, Adventure", ReleaseYear = 2012, Director = "Joss Whedon", Picture = "https://media.s-bol.com/m7krYyxWjlz9/3lNORVA/550x781.jpg", Trailer = "https://www.youtube.com/watch?v=eOrNdBpGMv8&ab_channel=MarvelEntertainment", FranchiseId = 1 },
                new Movie { Id = 3, Title = "Harry Potter and the Goblet of Fire", Genre = "Fantasy, Adventure", ReleaseYear = 2005, Director = "Mike Newell", Picture = "https://images.pathe-thuis.nl/15295_450x640.jpg", Trailer = "https://www.youtube.com/watch?v=3EGojp4Hh6I&ab_channel=MovieclipsClassicTrailers", FranchiseId = 2 }
            };
            return movies;
        }
    }
}
