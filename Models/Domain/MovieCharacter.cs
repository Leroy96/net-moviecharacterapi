﻿namespace MovieCharacterAPI.Models.Domain
{
    public class MovieCharacter
    {
        // Relationships 
        public int MovieId { get; set; }
        public Movie Movie { get; set; }
        public int CharacterId { get; set; }
        public Character Character { get; set; }
    }
}
