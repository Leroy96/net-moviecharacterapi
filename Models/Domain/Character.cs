﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Models.Domain
{
    public class Character
    {
        // Primary key (PK)
        public int Id { get; set; }
        // Fields
        [MaxLength(50)] public string Name { get; set; }
        [MaxLength(50)] public string Alias { get; set; }
        [MaxLength(20)] public string Gender { get; set; }
        public string Picture { get; set; }
        // Relationships
        public ICollection<Movie> Movies { get; set; }
    }
}
