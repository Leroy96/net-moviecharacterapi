﻿using System.Collections.Generic;

namespace MovieCharacterAPI.Models.Domain
{
    public class Franchise
    {
        // Primary key (PK)
        public int Id { get; set; }
        // Fields
        public string Name { get; set; }
        public string Description { get; set; }
        // Relationships
        public ICollection<Movie> Movies { get; set; }
    }
}
