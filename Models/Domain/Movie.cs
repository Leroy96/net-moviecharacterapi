﻿using System.Collections.Generic;

namespace MovieCharacterAPI.Models.Domain
{
    public class Movie
    {
        // Primary Key (PK)
        public int Id { get; set; }
        // Fields
        public string Title { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
        // Relationships
        public int FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
        public ICollection<Character> Characters { get; set; }
    }
}
