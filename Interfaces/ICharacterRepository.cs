﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MovieCharacterAPI.DTOs.CharacterDTOs;
using MovieCharacterAPI.Models.Domain;

namespace MovieCharacterAPI.Interfaces
{
    public interface ICharacterRepository
    {
        /// <summary>
        /// Abstract method to get all Characters.
        /// </summary>
        /// <returns></returns>
        Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersAsync();

        /// <summary>
        /// Abstract method to get a Character by ID.
        /// </summary>
        /// <param name="id">ID of the Character</param>
        /// <returns></returns>
        Task<ActionResult<CharacterReadDTO>> GetCharactersAsync(int id);

        /// <summary>
        /// Abstract method to create a Character
        /// </summary>
        /// <param name="characterCreateDTO">The new Character object</param>
        /// <returns></returns>
        Task<ActionResult<IEnumerable<CharacterReadDTO>>> PostCharacterAsync(CharacterCreateDTO characterCreateDTO);

        /// <summary>
        /// Abstract method to update a Character
        /// </summary>
        /// <param name="id">ID of the Character</param>
        /// <returns></returns>
        Task<ActionResult<IEnumerable<CharacterUpdateDTO>>> PutCharacterAsync(int id);

        /// <summary>
        /// Abstract method to update a Character
        /// </summary>
        /// <param name="id">ID of the Character</param>
        /// <returns></returns>
        Task<ActionResult<IEnumerable<CharacterDeleteDTO>>> DeleteCharacterAsync(int id);
    }
}
