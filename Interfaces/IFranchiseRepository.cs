﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MovieCharacterAPI.DTOs.FranchiseDTOs;
using MovieCharacterAPI.Models.Domain;

namespace MovieCharacterAPI.Interfaces
{
    public interface IFranchiseRepository
    {
        /// <summary>
        /// Abstract method to get all Franchises.
        /// </summary>
        /// <returns></returns>
        Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchisesAsync();

        /// <summary>
        /// Abstract method to get a Franchise by ID.
        /// </summary>
        /// <param name="id">ID of the Franchise</param>
        /// <returns></returns>
        Task<ActionResult<FranchiseReadDTO>> GetFranchisesAsync(int id);

        /// <summary>
        /// Abstract method to create a Franchise
        /// </summary>
        /// <param name="franchiseCreateDTO">The new Franchise object</param>
        /// <returns></returns>
        Task<ActionResult<IEnumerable<FranchiseReadDTO>>> PostFranchiseAsync(FranchiseCreateDTO franchiseCreateDTO);

        /// <summary>
        /// Abstract method to update a Franchise
        /// </summary>
        /// <param name="id">ID of the Franchise</param>
        /// <returns></returns>
        Task<ActionResult<IEnumerable<FranchiseUpdateDTO>>> PutFranchiseAsync(int id);

        /// <summary>
        /// Abstract method to update a Franchise
        /// </summary>
        /// <param name="id">ID of the Franchise</param>
        /// <returns></returns>
        Task<ActionResult<IEnumerable<FranchiseDeleteDTO>>> DeleteFranchiseAsync(int id);
    }
}
