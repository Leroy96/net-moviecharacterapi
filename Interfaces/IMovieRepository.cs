﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using MovieCharacterAPI.DTOs.MovieDTOs;
using MovieCharacterAPI.Models.Domain;

namespace MovieCharacterAPI.Interfaces
{
    public interface IMovieRepository
    {
        /// <summary>
        /// Abstract method to get all Movies.
        /// </summary>
        /// <returns></returns>
        Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMoviesAsync();

        /// <summary>
        /// Abstract method to get a Movie by ID.
        /// </summary>
        /// <param name="id">ID of the Movie</param>
        /// <returns></returns>
        Task<ActionResult<MovieReadDTO>> GetMoviesAsync(int id);

        /// <summary>
        /// Abstract method to get all Characters in a Movie.
        /// </summary>
        /// <param name="id">ID of the Movie</param>
        /// <returns></returns>
        Task<ActionResult<IEnumerable<Character>>> GetAllCharactersInMovieAsync(int id);

        /// <summary>
        /// Abstract method to create a movie
        /// </summary>
        /// <param name="movieCreateDTO">The new Movie object</param>
        /// <returns></returns>
        Task<ActionResult<IEnumerable<MovieReadDTO>>> PostMovieAsync(MovieCreateDTO movieCreateDTO);

        /// <summary>
        /// Abstract method to update a movie
        /// </summary>
        /// <param name="id">ID of the Movie</param>
        /// <returns></returns>
        Task<ActionResult<IEnumerable<MovieUpdateDTO>>> PutMovieAsync(int id);

        /// <summary>
        /// Abstract method to update a movie
        /// </summary>
        /// <param name="id">ID of the Movie</param>
        /// <returns></returns>
        Task<ActionResult<IEnumerable<MovieDeleteDTO>>> DeleteMovieAsync(int id);
    }
}
