﻿using AutoMapper;
using MovieCharacterAPI.DTOs.CharacterDTOs;
using MovieCharacterAPI.Models.Domain;

namespace MovieCharacterAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>();

            // Post
            CreateMap<CharacterCreateDTO, Character>().ReverseMap();
        }
    }
}
 