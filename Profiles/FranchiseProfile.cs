﻿using AutoMapper;
using MovieCharacterAPI.DTOs.FranchiseDTOs;
using MovieCharacterAPI.Models.Domain;

namespace MovieCharacterAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>();

            // Post
            CreateMap<FranchiseCreateDTO, Franchise>().ReverseMap();
        }
    }
}
