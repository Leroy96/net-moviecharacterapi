﻿using AutoMapper;
using MovieCharacterAPI.DTOs.MovieDTOs;
using MovieCharacterAPI.Models.Domain;

namespace MovieCharacterAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>();

            // Post
            CreateMap<MovieCreateDTO, Movie>().ReverseMap();
        }
    }
}