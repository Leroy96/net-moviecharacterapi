﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.DTOs.FranchiseDTOs;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.Domain;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class FranchisesController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public FranchisesController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Franchises
        /// <summary>
        /// Gets all franchises from the Database.
        /// </summary>
        /// <returns>A list of Franchise DTO objects.</returns>
        /// <response code="200"></response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchisesAsync()
        {
            var domainFranchises = await _context.Franchises.ToListAsync();
            var dtoFranchise = _mapper.Map<List<FranchiseReadDTO>>(domainFranchises);
            return dtoFranchise;
        }

        // GET: api/Franchises/5
        /// <summary>
        /// Gets a franchise from the Database by ID.
        /// </summary>
        /// <param name="id">ID of the franchise you want to get.</param>
        /// <returns>A Franchise DTO object.</returns>
        /// <response code="200"></response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchiseAsync(int id)
        {
            var domainFranchise = await _context.Franchises.FindAsync(id);

            if (domainFranchise == null)
            {
                return NotFound();
            }

            var dtoFranchiseRead = _mapper.Map<FranchiseReadDTO>(domainFranchise);

            return dtoFranchiseRead;
        }

        // PUT: api/Franchises/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Create or replace a record in the Database
        /// </summary>
        /// <param name="id">ID of the franchise you want to create/replace.</param>
        /// <param name="franchise">Franchise you want to create/replace.</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}")]
        public async Task<ActionResult> PutFranchiseAsync(int id, Franchise franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest();
            }

            _context.Entry(franchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Franchises
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Create a record in the Database
        /// </summary>
        /// <param name="franchise">Franchise you want to create.</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchiseAsync(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchise", new { id = franchise.Id }, franchise);
        }

        // DELETE: api/Franchises/5
        /// <summary>
        /// Delete a record from the Database
        /// </summary>
        /// <param name="id">ID of the franchise you want to delete.</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<ActionResult<FranchiseDeleteDTO>> DeleteFranchiseAsync(int id)
        {
            var domainFranchise = await _context.Franchises.FindAsync(id);
            var dtoFranchiseDelete = _mapper.Map<FranchiseDeleteDTO>(domainFranchise);

            _context.Franchises.Remove(domainFranchise);
            await _context.SaveChangesAsync();

            return dtoFranchiseDelete;
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }
    }
}
