﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.Domain;
using AutoMapper;
using MovieCharacterAPI.DTOs.CharacterDTOs;
using System.Net.Mime;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class CharactersController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public CharactersController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Characters
        /// <summary>
        /// Gets all characters from the Database.
        /// </summary>
        /// <returns>A list of Character DTO objects.</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersASync()
        {
            var domainCharacters = await _context.Characters.ToListAsync();
            var dtoCharacters = _mapper.Map<List<CharacterReadDTO>>(domainCharacters);
            return dtoCharacters;
        }

        // GET: api/Characters/5
        /// <summary>
        /// Gets a character from the Database by ID.
        /// </summary>
        /// <param name="id">ID of the character you want to get.</param>
        /// <returns>A Character DTO object.</returns>
        /// <response code="200"></response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacterAsync(int id)
        {
            var domainCharacter = await _context.Characters.FindAsync(id);

            if (domainCharacter == null)
            {
                return NotFound();
            }

            var dtoCharacterRead = _mapper.Map<CharacterReadDTO>(domainCharacter);

            return dtoCharacterRead;
        }

        // PUT: api/Characters/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Create or replace a record in the Database
        /// </summary>
        /// <param name="id">ID of the franchise you want to create/replace.</param>
        /// <param name="character">Character you want to create/replace.</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}")]
        public async Task<ActionResult> PutCharacterAsync(int id, Character character)
        {
            if (id != character.Id)
            {
                return BadRequest();
            }

            _context.Entry(character).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Characters
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Create a record in the Database
        /// </summary>
        /// <param name="characterDto">Character you want to create.</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost]
        public async Task<ActionResult<CharacterReadDTO>> PostCharacterAsync(CharacterCreateDTO characterDto)
        {
            var domainCharacter = _mapper.Map<Character>(characterDto);

            _context.Characters.Add(domainCharacter);
            await _context.SaveChangesAsync();

            CharacterReadDTO newCharacterDto = _mapper.Map<CharacterReadDTO>(domainCharacter);

            return CreatedAtAction("GetCharacter", new { id = newCharacterDto.Id }, newCharacterDto);
        }

        // DELETE: api/Characters/5
        /// <summary>
        /// Delete a record from the Database by ID
        /// </summary>
        /// <param name="id">ID of the franchise you want to delete.</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<ActionResult<CharacterDeleteDTO>> DeleteCharacterAsync(int id)
        {
            var domainCharacter = await _context.Characters.FindAsync(id);
            var dtoCharacterDelete = _mapper.Map<CharacterDeleteDTO>(domainCharacter);
            
        
            _context.Characters.Remove(domainCharacter);
            await _context.SaveChangesAsync();

            return dtoCharacterDelete;
        }

        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.Id == id);
        }
    }
}
