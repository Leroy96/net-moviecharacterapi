﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.Domain;
using AutoMapper;
using MovieCharacterAPI.DTOs.MovieDTOs;
using System.Net.Mime;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class MoviesController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public MoviesController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Movies
        /// <summary>
        /// Gets all movies from the Database.
        /// </summary>
        /// <returns>A list of Movie DTO objects.</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMoviesAsync()
        {
            var domainMovies = await _context.Movies.ToListAsync();
            var dtoMovies = _mapper.Map<List<MovieReadDTO>>(domainMovies);
            return dtoMovies;
        }

        // GET: api/Movies/5
        /// <summary>
        /// Gets a movie from the Database by ID.
        /// </summary>
        /// <param name="id">ID of the movie you want to get.</param>
        /// <returns>A Movie DTO object.</returns>
        /// <response code="200"></response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovieAsync(int id)
        {
            var domainMovie = await _context.Movies.FindAsync(id);

            if (domainMovie == null)
            {
                return NotFound();
            }

            var dtoMovieRead = _mapper.Map<MovieReadDTO>(domainMovie);

            return dtoMovieRead;
        }

        // PUT: api/Movies/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Update a record in the Database.
        /// </summary>
        /// <param name="id">ID of the movie you want to update.</param>
        /// <param name="movie">Movie you want to update.</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}")]
        public async Task<ActionResult> PutMovieAsync(int id, Movie movie)
        {
            if (id != movie.Id)
            {
                return BadRequest();
            }

            _context.Entry(movie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Movies
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Create a record in the Database
        /// </summary>
        /// <param name="movie">Movie you want to add.</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovie", new { id = movie.Id }, movie);
        }

        // DELETE: api/Movies/5
        /// <summary>
        /// Delete a record in the Database
        /// </summary>
        /// <param name="id">ID of the movie you want to delete.</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<ActionResult<MovieDeleteDTO>> DeleteMovieAsync(int id)
        {
            var domainMovie = await _context.Movies.FindAsync(id);
            var dtoMovieDelete = _mapper.Map<MovieDeleteDTO>(domainMovie);

            _context.Movies.Remove(domainMovie);
            await _context.SaveChangesAsync();

            return dtoMovieDelete;
        }

        private bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }
    }
}
